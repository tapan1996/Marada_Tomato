<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Marada';
        $user->email = 'admin@admin.com';
        $user->password = Hash::make('secret');
        $user->group_id = 1;
        $user->address = 'Kyjov 123';
        $user->city = 'Brno';
        $user->ico = 234234;
        $user->dic = 2343234;
        $user->phone = 2343234;
        $user->postal = 23444;
        $user->country = 'Česká republika';
        $user->is_admin = true;
        $user->save();

    }
}
