<?php

namespace Database\Seeders;

use App\Models\DeliveryDay;
use App\Models\Order;
use App\Models\OrderItem;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order = new Order();
        $order->user_id = 1;
        $order->date = Carbon::now();
        $order->price = 500;
        $order->price_full = 600;
        $order->save();
        $order = new Order();
        $order->user_id = 1;
        $order->date = Carbon::now();
        $order->price = 400;
        $order->price_full = 600;
        $order->save();

        $io = new OrderItem();
        $io->item_id = 1;
        $io->order_id = 1;
        $io->amount = 5;
        $io->price = 40;
        $io->price_full = 50;
        $io->save();
        $io = new OrderItem();
        $io->item_id = 2;
        $io->order_id = 1;
        $io->amount = 1;
        $io->price = 70;
        $io->price_full = 70;
        $io->save();
        $io = new OrderItem();
        $io->item_id = 3;
        $io->order_id = 1;
        $io->amount = 1;
        $io->price = 40;
        $io->price_full = 50;
        $io->save();
        $io = new OrderItem();
        $io->item_id = 1;
        $io->order_id = 2;
        $io->amount = 8;
        $io->price = 60;
        $io->price_full = 70;
        $io->save();
        $io = new OrderItem();
        $io->item_id = 3;
        $io->order_id = 2;
        $io->amount = 7;
        $io->price = 30;
        $io->price_full = 50;
        $io->save();


        $dd = new DeliveryDay();
        $dd->date = Carbon::now();
        $dd->save();
    }
}
