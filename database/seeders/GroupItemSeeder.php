<?php

namespace Database\Seeders;

use App\Models\GroupItem;
use Illuminate\Database\Seeder;

class GroupItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $group_item = new GroupItem();
        $group_item->group_id = 1;
        $group_item->item_id = 1;
        $group_item->price = 100;
        $group_item->price_full = 121;
        $group_item->save();
        $group_item = new GroupItem();
        $group_item->group_id = 1;
        $group_item->item_id = 2;
        $group_item->price = 100;
        $group_item->price_full = 121;
        $group_item->save();
        $group_item = new GroupItem();
        $group_item->group_id = 1;
        $group_item->item_id = 3;
        $group_item->price = 100;
        $group_item->price_full = 121;
        $group_item->save();


        $group_item = new GroupItem();
        $group_item->group_id = 4;
        $group_item->item_id = 1;
        $group_item->price = 100;
        $group_item->price_full = 121;
        $group_item->save();
        $group_item = new GroupItem();
        $group_item->group_id = 4;
        $group_item->item_id = 2;
        $group_item->price = 100;
        $group_item->price_full = 121;
        $group_item->save();
        $group_item = new GroupItem();
        $group_item->group_id = 4;
        $group_item->item_id = 3;
        $group_item->price = 100;
        $group_item->price_full = 121;
        $group_item->save();


        $group_item = new GroupItem();
        $group_item->group_id = 2;
        $group_item->item_id = 1;
        $group_item->price = 100;
        $group_item->price_full = 121;
        $group_item->save();
        $group_item = new GroupItem();
        $group_item->group_id = 2;
        $group_item->item_id = 2;
        $group_item->price = 100;
        $group_item->price_full = 121;
        $group_item->save();
        $group_item = new GroupItem();
        $group_item->group_id = 2;
        $group_item->item_id = 3;
        $group_item->price = 100;
        $group_item->price_full = 121;
        $group_item->save();


        $group_item = new GroupItem();
        $group_item->group_id = 3;
        $group_item->item_id = 1;
        $group_item->price = 100;
        $group_item->price_full = 121;
        $group_item->save();
        $group_item = new GroupItem();
        $group_item->group_id = 3;
        $group_item->item_id = 2;
        $group_item->price = 100;
        $group_item->price_full = 121;
        $group_item->save();
        $group_item = new GroupItem();
        $group_item->group_id = 3;
        $group_item->item_id = 3;
        $group_item->price = 100;
        $group_item->price_full = 121;
        $group_item->save();


    }
}
