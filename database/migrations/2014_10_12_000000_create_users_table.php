<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('hash')->nullable();
            $table->boolean('is_admin')->default(false);
            $table->string('group_id')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('ico')->nullable();
            $table->string('dic')->nullable();
            $table->string('phone')->nullable();
            $table->string('postal')->nullable();
            $table->string('country')->nullable();
            $table->string('type')->nullable();
            $table->string('company')->nullable();
            $table->integer('dealer_id')->nullable();
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
