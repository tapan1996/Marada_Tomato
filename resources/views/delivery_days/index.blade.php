@extends('layout.layout')

@section('content')
    <div class="card">
        <div class="card-header">
            <b>Dny dovozu</b>
        </div>
        <form class="w-100 m-2" action="/delivery_days/store"
              method="post"
        >
            @csrf
            <input class="w-25 ml-2" name="date" type="date" required style="height: 45px;">
            <input type="submit" value="Přidat den dovozu" class="btn btn-success" style="vertical-align: top!important;">

        </form>

    <div class="card-body pt-0">
        <table class="table table-responsive-md">
            <thead>
            <tr>
                <th class="border-top-0">Den dovozu</th>
                <th class="border-top-0">Akce</th>
            </tr>
            </thead>

            <tbody>
            @foreach($delivery_days as $delivery_day)
                <tr>
                    <td>{{$delivery_day->date}}</td>
                    <td>
                        <form action="/delivery_days/delete/{{$delivery_day->id}}"
                              method="post"
                        >
                            @csrf
                            <input type="submit" value="Odstranit" class="btn btn-danger">

                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>

        </table>

    </div>





@endsection
