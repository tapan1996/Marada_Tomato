@extends('layout.layout')

@section('content')

    <div class="container mt-5" style="width: 100%;">
        <div class="card w-100">
            <div class="card-header">
                <b>Vytvoření skupiny</b>
            </div>
            <div class="card-body" style="text-align: left">
                <form action="/groups/store" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Jméno</label>
                        <input class="form-control" name="name">
                    </div>
                    <input class="form-control btn-success" type="submit" value="Vytvořit skupinu">

                </form>

            </div>
        </div>
    </div>

@endsection
