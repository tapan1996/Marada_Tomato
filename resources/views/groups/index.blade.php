@extends('layout.layout')
@section('content')
    <div class="card">
        <div class="card-header d-flex">
            <b>Správa skupin</b><a href="/groups/create" class="float-right ml-auto btn btn-success">Vytvořit novou
                skupinu</a>
        </div>
        <div class="card-body mt-0 pt-0 ">
            <table class="table table-responsive-md">
                @foreach($groups as $group)
                    <tr>
                        <td>{{$group->name}}</td>
                        <td>
                            @foreach($group->users as $user)
                                {{$user->name}}<br>
                            @endforeach
                        </td>
                        <td class="text-right">
                            <a class="btn btn-warning" href="/groups/edit/{{$group->id}}">Upravit</a>
                        </td>
                    </tr>

                @endforeach
            </table>
        </div>
    </div>
@endsection
