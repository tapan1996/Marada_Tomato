@extends('layout.layout')

@section('content')
    <div class="container mt-5" style="width: 100%;">
        <div class="card w-100">
            <div class="card-header">
                <b>Úprava údajů</b>
            </div>
            <form class="card-body" action="/groups/update/{{$group->id}}" method="POST">
                @csrf
                <div class="form-group">
                    <label>Jméno</label>
                    <input class="form-control" value="{{$group->name}}" name="name">
                </div>

                @foreach($items as $item)
                    <div class="form-group">
                        <label>Cena pro: {{$item->name}} (s DPH) </label>
                        <input required class="form-control" value="{{$item_prices[$item->id]}}" type="number"
                               name="item_price[{{$item->id}}]">
                    </div>
                @endforeach

                <div class="form-group">
                    <input class="form-control btn-success" type="submit" value="Upravit skupinu">
                </div>
            </form>
        </div>
    </div>

@endsection
