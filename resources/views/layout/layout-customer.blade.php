<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layout.head')
</head>
<body>

@include('layout.menu')
{{--@include('layout.info-panel')--}}

<?php
use Illuminate\Support\Facades\Auth;
$user = Auth::user();
?>

<div id="app">
    <div class="container mb-5">
        <div class="toggled d-inline">
            @yield('content')
        </div>
    </div>
</div>


@include('layout.footer')
{{--</div>--}}
<script src="{{asset('/js/app.js')}}"></script>
@include('layout.scripts')
<style>/* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }</style>

</body>

</html>
