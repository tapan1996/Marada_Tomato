<!DOCTYPE html>
<html class="h-100" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layout.head')
</head>
<body>

<?php
use Illuminate\Support\Facades\Auth;
$user = Auth::user();
?>

{{--@include('layout.menu')--}}

<div id="app">
    <div class="d-flex" id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper" style="box-shadow: 0 0 10px black">
            <div class="sidebar-heading">Objednávkový systém</div>
            <div class="list-group list-group-flush">
                <a class="list-group-item list-group-item-action" href="/users">Správa uživatelů</a>
                <a class="list-group-item list-group-item-action" href="/groups">Správa skupin</a>
                <a class="list-group-item list-group-item-action" href="/items">Správa položek</a>
                <a class="list-group-item list-group-item-action" href="/delivery_days">Dny dovozu</a>
                <a class="list-group-item list-group-item-action" href="/orders">Přehled objednávek</a>
            </div>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <nav class="navbar navbar-expand navbar-dark"
                 style=" box-shadow: 0 10px 10px -10px black">
                <button class="btn btn-success d-md-none d-block" id="menu-toggle">Zobrazit menu</button>

{{--                <button class="navbar-toggler"--}}
{{--                        type="button"--}}
{{--                        data-toggle="collapse"--}}
{{--                        data-target="#navbarSupportedContent"--}}
{{--                        aria-controls="navbarSupportedContent"--}}
{{--                        aria-expanded="false"--}}
{{--                        aria-label="Toggle navigation">--}}
{{--                    <span class="navbar-toggler-icon"></span>--}}
{{--                </button>--}}

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto ">
                        <form id="logout-form" action="{{ route('logout') }}" method="POST">
                            @csrf
                            <input style="float: right;" class="btn-success btn" type="submit" value="Odhlásit">
                        </form>
                    </ul>
                </div>
            </nav>

            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->
    @include('layout.footer')
</div>


<script src="{{asset('/js/app.js')}}"></script>
@include('layout.scripts')
<style>/* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }</style>

</body>

</html>
