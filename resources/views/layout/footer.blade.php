<section class="w-100 footer fixed-bottom" style="background-color: transparent; z-index: 1000;
    bottom: 0;">
    <footer class="container-fluid mt-2">
        <div class="row">
            <div class="col-6 text-left">
                <p>&copy; {{date("Y")}} Marada e-shop</p>
            </div>
            <div class=" col-6 text-right">
                <p>Vytvořili <a href="//softici.cz">Softíci.cz</a> <img class="badge-logo" style="height: 20px;"
                                                                        src="{{asset('/images/softici_badge_lg.png')}}">
                </p>
            </div>
        </div>
    </footer>
</section>
