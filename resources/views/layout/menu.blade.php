<nav class="navbar navbar-dark fixed-top navbar-toggleable-md bg-faded mb-5 py-3" style="box-shadow: 0 0 10px black; background: #1f1f1f">
    <div class="navbar-brand">
        <a href="/">Objednávkový systém</a>
    </div>

    <form id="logout-form" action="{{ route('logout') }}" method="POST">
        @csrf
        <input style="float: right;" class="btn-success btn" type="submit" value="Odhlásit">
    </form>
</nav>

