@extends('layout.layout')

@section('content')
    <div class="card">
        <div class="card-header d-flex">
            <b>Správa uživatelů</b><a href="/users/create" class="float-right ml-auto btn btn-success">Vytvořit nového
                uživatele</a>
        </div>

        <div class="card-body mt-0 pt-0 ">
            <table class="table table-responsive-md">
                <tr>
                    <th>
                        Jméno
                    </th>
                    <th>
                        Skupina
                    </th>
                    <th>
                        Dealer
                    </th>
                    <th class="text-center">

                    </th>
                </tr>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->name}}</td>
                        {{--                    <td class="text-right"><a class="btn btn-warning" href="/users/edit/{{$user->id}}">Upravit</a></td>--}}
                        <td>
                            {{$user->group->name}}
                        </td>
                        <td>
                            @if($user->dealer_id)
                                {{\App\Models\User::findOrFail($user->dealer_id)->name}}

                            @endif
                        </td>
                        <td class="text-right">
                            <a class="btn btn-warning" href="/users/edit/{{$user->id}}">Upravit</a>
                            @if($user->deleted_at)
                                <a class="btn btn-warning"
                                   href="/users/activate/{{$user->id}}">Aktivovat</a>

                            @else
                                <a class="btn btn-warning"
                                   href="/users/delete/{{$user->id}}">Deaktivovat</a>

                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

@endsection
