@extends('layout.layout')

@section('content')

    <div class="container mt-5" style="width: 100%;">
        <div class="card w-100">
            <div class="card-header">
                <b>Vytvoření uživatele</b>
            </div>

            <div class="card-body"
                 style="text-align: left">
                <form method="post" action="/users/store/">
                    @csrf
                    <div class="form-group">
                        <label>Jméno</label><input class="form-control" class="form-group"
                                                   type="text" name="name"
                                                   required>
                    </div>
                    <div class="form-group">


                        <div class="form-group">
                            <label>Email</label><input class="form-control" class="form-group" type="email"
                                                       name="email" required></div>

                    </div>
                    <div class="form-group">

                        <label>Skupina</label>
                        <select required name="group_id" class="form-control">
                            <option value="{{null}}"> Vyberte skupinu</option>
                            @foreach($groups as $group)
                                <option value="{{$group->id}}">{{$group->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <input class="btn btn-success" type="submit" value="Uložit">
                    </div>
                    <div class="form-group">

                        <label>Vyberte dealera</label>
                        <select name="dealer_id" class="form-control">
                            <option value="{{null}}"> Vyberte dealera</option>
                            @foreach($users as $user)
                                <option value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <input class="btn btn-success" type="submit" value="Uložit">
                    </div>

                </form>

            </div>
        </div>
    </div>


@endsection
