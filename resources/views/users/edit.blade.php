@extends('layout.layout')

@section('content')
    <div class="container mt-5" style="width: 100%;">
        <div class="card w-100">
            <div class="card-header">
                <b>Úprava údajů</b>
            </div>
            <form class="card-body" action="/users/update/{{$user->id}}"
                  method="POST">

                @csrf
                <div class="form-group">
                    <label>Uživatelské jméno</label> <input class="form-control" name="name" value="{{$user->name}}"
                                                            required>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" type="email" name="email" value="{{$user->email}}" required>
                </div>

                <div class="form-group">

                    <label>Skupina</label>
                    <select name="group_id" class="form-control">
                        @foreach($groups as $group)
                            <option
                                value="{{$group->id}}" {{$user->group_id == $group->id ? 'selected=true':''}}>{{$group->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">

                    <label>Vyberte dealera</label>
                    <select name="dealer_id" class="form-control">
                        <option value="{{null}}"> Vyberte dealera</option>
                        @foreach($dealers as $dealer)
                            <option
                                value="{{$dealer->id}}" {{$dealer->id == $user->dealer_id ? 'selected=true':''}}>{{$dealer->name}}
                            </option>
                        @endforeach
                    </select>
                </div>


                <div class="form-group">
                    <label>Adresa</label> <input class="form-control" name="address" value="{{$user->address}}"
                                                 required></div>
                <div class="form-group">
                    <label>Město</label> <input class="form-control" name="city" value="{{$user->city}}" required></div>
                <div class="form-group">
                    <label>PSČ</label> <input class="form-control" name="postal" value="{{$user->postal}}" required>
                </div>
                <div class="form-group">
                    <label>Stát</label> <input class="form-control" name="country" value="{{$user->country}}" required>
                </div>
                <div class="form-group">
                    <label>Jméno firmy</label> <input class="form-control" name="company" value="{{$user->company}}">
                </div>
                <div class="form-group">
                    <label>IČO</label> <input class="form-control" name="ico" value="{{$user->ico}}"></div>
                <div class="form-group">
                    <label>DIČ</label> <input class="form-control" name="dic" value="{{$user->dic}}"></div>
                <div class="form-group">
                    <label>Telefon</label> <input class="form-control" name="phone" value="{{$user->phone}}" required>
                </div>
                <div class="form-group">

                    <input class="form-control btn-success" type="submit" value="Upravit uživatele"></div>
            </form>
        </div>
    </div>
@endsection
