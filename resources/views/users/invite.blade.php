@extends('layout.layout-customer')

@section('content')

    <div class="container mt-5" style="width: 100%;">
        <div class="card w-100">
            <div class="card-header">
                <b>Vytvoření uživatele </b>
            </div>

            <div class="card-body"
                 style="text-align: left">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form method="post" action="/users/customer/{{$user->hash}}">
                    @csrf
                    <div class="form-group">
                        <label>IČO</label> <input class="form-control" type="text" name="ico" placeholder="01234567"
                                                  required>

                    </div>
                    <div class="form-group">

                        <input class="form-control btn-success" type="submit" value="Získat data z aresu">
                    </div>
                </form>


                <form method="post" action="/users/customer/{{$user->hash}}/submit">
                    @csrf
                    <div class="form-group">

                        @if(isset($ares))
                            <div class="form-group">
                                <label>Uživatelské jméno</label><input class="form-control" class="form-group"
                                                                       type="text" name="name"
                                                                       value="{{$ares['name'].' '.$ares['surname']}}"
                                                                       required>
                            </div>

                        @else
                            <div class="form-group">
                                <label>Uživatelské jméno</label><input class="form-control" class="form-group"
                                                                       type="text" name="name"
                                                                       required></div>

                        @endif
                        <div class="form-group">
                            <label>Email</label><input class="form-control" class="form-group" type="email"
                                                       value="{{$user['email']}}"
                                                       name="email" required></div>

                    </div>

                    <div class="form-group">
                        @if(isset($ares))
                            <div class="form-group">
                                <label>Typ uživatele</label>
                                <select class="form-control" name="type">
                                    @if($ares['type'] == 0)
                                        <option value="0" selected="selected">Fyzicka osoba</option>
                                    @else
                                        <option value="0">Fyzicka osoba</option>
                                    @endif
                                    @if($ares['type'] == 2)
                                        <option value="2" selected="selected">Pravnicka osoba</option>
                                    @else
                                        <option value="2">Pravnicka osoba</option>
                                    @endif
                                    @if($ares['type'] == 1)
                                        <option value="1" selected="selected">Podnikatel</option>
                                    @else
                                        <option value="1" selected="selected">Podnikatel</option>
                                    @endif
                                </select>
                            </div>


                            <div class="form-group">
                                <label>Adresa</label> <input class="form-control" type="text" name="address"
                                                             value="{{$ares['street']}}"
                                                             placeholder="Palackého 48/2 778 00" required>
                            </div>

                            <div class="form-group">
                                <label>Město</label><input class="form-control" type="text" name="city"
                                                           value="{{$ares['town']}}"
                                                           placeholder="Praha" required>
                            </div>

                            <div class="form-group">
                                <label>PSČ</label><input class="form-control" type="text" name="postal"
                                                         value="{{$ares['postal']}}"
                                                         placeholder="123456" required>
                            </div>

                            <div class="form-group">
                                <label>Stát</label><input class="form-control" type="text" name="country"
                                                          value="{{$ares['country']}}"
                                                          placeholder="Česká republika" required>
                            </div>

                            <div class="form-group">
                                <label>Jméno firmy</label> <input class="form-control" type="text"
                                                                  value="{{$ares['company']}}"
                                                                  name="company_name"
                                                                  placeholder="XY s.r.o.">
                            </div>

                            <div class="form-group">
                                <label>IČO </label><br> <input class="form-control" type="text" name="ico"
                                                               value="{{$ares['ic']}}"
                                                               placeholder="01234567">
                            </div>

                            <div class="form-group">
                                <label>DIČ</label> <input class="form-control" type="text" name="dic"
                                                          value="{{$ares['dic']}}"
                                                          placeholder="Neplátce">
                            </div>

                        @else
                            <div class="form-group">
                                <label>Typ uživatele</label>
                                <select class="form-control" name="type">
                                    <option value="0" selected="selected">Fyzicka osoba</option>
                                    <option value="1">Pravnicka osoba</option>
                                    <option value="2">Podnikatel</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Adresa</label> <input class="form-control" type="text" name="address"
                                                             placeholder="Palackého 48/2 778 00"
                                                             required>
                            </div>

                            <div class="form-group">
                                <label>Město</label><input class="form-control" type="text" name="city"
                                                           placeholder="Praha" required>
                            </div>

                            <div class="form-group">
                                <label>PSČ</label><br><input class="form-control" type="text" name="postal"
                                                             placeholder="123456"
                                                             required>
                            </div>

                            <div class="form-group">
                                <label>Stát</label><br><input class="form-control" type="text" name="country"
                                                              placeholder="Česká republika"
                                                              required>
                            </div>

                            <div class="form-group">
                                <label>Jméno firmy</label> <input class="form-control" type="text"
                                                                  name="company_name"
                                                                  placeholder="XY s.r.o.">
                            </div>

                            <div class="form-group">
                                <label>IČO</label><br> <input class="form-control" type="text" name="ico"
                                                              placeholder="01234567"
                                >
                            </div>

                            <div class="form-group">
                                <label>DIČ</label><br> <input class="form-control" type="text" name="dic"
                                                              placeholder="Neplátce">
                            </div>
                        @endif


                        <div id="show" style="display: none">
                            <div class="form-group">
                                <label>Adresa</label> <input class="form-control" type="text" name="da_address"
                                                             placeholder="Palackého 48/2 778 00">
                            </div>

                            <div class="form-group">
                                <label>Město</label><input class="form-control" type="text" name="da_city"
                                                           placeholder="Praha">
                            </div>

                            <div class="form-group">
                                <label>PSČ</label><br><input class="form-control" type="text" name="da_postal"
                                                             placeholder="123456">
                            </div>
                        </div>


                        <div class="form-group">
                            <label>Telefon</label> <input class="form-control" type="text" name="phone"
                                                          placeholder="777 777 777"
                                                          required>
                        </div>


                        <div class="form-group">
                            <label>Heslo</label> <input class="form-control" type="password" name="password"
                                                        placeholder="*****"
                                                        required>
                        </div>


                        <div class="form-group">
                            <label>Heslo podruhé</label> <input class="form-control" type="password"
                                                                name="password_2"
                                                                placeholder="*****"
                                                                required>
                        </div>

                        <div class="form-group">
                            <input class="form-control btn-success" type="submit" value="Potvrdit">
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>


@endsection
