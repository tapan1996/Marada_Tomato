@extends('layout.layout-customer')

@section('content')

    <customer :orders="{{$orders}}" :items="{{$items}}" :dates="{{$dates}}"></customer>
@endsection
