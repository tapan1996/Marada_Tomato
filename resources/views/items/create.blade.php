@extends('layout.layout')

@section('content')
    <div class="container mt-5" style="width: 100%;">
        <div class="card w-100">
            <div class="card-header">
                <b>Vytvoření položky</b>
            </div>
            <div class="card-body" style="text-align: left">


    <form action="/items/store"
          enctype="multipart/form-data"
          method="POST">

        @csrf
        <div class="form-group">
        <label>Jméno</label>
        <input class="form-control" name="name">
        </div>
        <div class="form-group">
            <label class="btn-success btn" for="file-upload"> Přidat obrázek
                <input id="file-upload" class="d-none" type="file" accept="jpg,png" name="photo">
            </label>
        </div>

        <input class="form-control btn-success" type="submit" value="Vytvořit položku">

    </form>


@endsection
