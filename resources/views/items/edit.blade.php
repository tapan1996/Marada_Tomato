@extends('layout.layout')

@section('content')
    <div class="container mt-5" style="width: 100%;">
        <div class="card w-100">
            <div class="card-header">
                <b>Úprava údajů</b>
            </div>

            <form class="card-body" action="/items/update/{{$item->id}}"
                  enctype="multipart/form-data"
                  method="POST">

                @csrf
                <div class="form-group">
                    <label>Jméno</label>
                    <input class="form-control"  name="name" value="{{$item->name}}">
                </div>
                <div class="form-group">
                @if($item->photo_path)
                    <img src="{{$item->photo_path}}">

                @endif
                    <label class="btn-success btn" for="file-upload"> Změnit obrázek
                <input id="file-upload" class="d-none" type="file" accept="jpg,png" name="photo">
                    </label>
                </div>
                <input class="form-control btn-success" type="submit" value="Upravit položku">

            </form>
        </div>
    </div>
@endsection
