@extends('layout.layout')

@section('content')

    <div class="card">
        <div class="card-header d-flex">
            <b>Správa položek</b> <a href="/items/create"  class="float-right ml-auto btn btn-success">Vytvořit novou položku</a>
        </div>
        <div class="card-body mt-0 pt-0 ">
            <table class="table table-responsive-md">
                @foreach($items as $item)
                    <tr>
                        @if($item->photo_path)
                        <td style="width: 5rem"><img style="height: 40px;" src="{{$item->photo_path}}"></td>
                            @else
                            <td></td>
                        @endif
                        <td>{{$item->name}}</td>
                        <td class="text-right"><a class="btn btn-warning" href="/items/edit/{{$item->id}}">Upravit</a></td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
