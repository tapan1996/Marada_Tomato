@extends('layout.layout')

@section('content')
    <div class="card">
        <div class="card-header">
            <b>Objednávky</b>
        </div>
        <div class="w-75 mr-auto ml-auto mt-2">
            <form method="POST">
                @csrf
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" style="width: 10rem">Vyberte den </span>
                    </div>

                    <select name="date" class="form-control h-auto">
                        <option value="{{null}}">Vyberte den</option>
                        @foreach($dates as $day)
                            <option
                                value="{{$day->date}}"
                                {{\Carbon\Carbon::make($day->date)->format('d.m.yy')== \Carbon\Carbon::make($date)->format('d.m.yy') ? 'selected=true':'' }}
                            >


                                {{\Carbon\Carbon::make($day->date)->format('d.m.yy')}}
                            </option>
                        @endforeach
                    </select>
                    {{--                    <input type="date" class="form-control" name="date" value="{{$date}}">--}}
                    <div class="input-group-append">
                        <button class="btn btn-success">Vyhledat</button>
                    </div>
                </div>
            </form>

            <div class="card border-dark">
                <div class="card-header border-dark d-flex" style="background-color: #b3b7bb">
                    <b>Denní součet  {{$date}}</b>
                    <form action="/pdf-create" method="post" class="float-right ml-auto">
                        @csrf

                        <input hidden name='date' value="{{$date}}">
                        <input class="btn btn-success" type="submit" value="Vytvořit PDF">
                    </form>
                </div>
                <div class="card-body mt-0 pt-0 border-dark">
                    <table class="table table-responsive-md">
                        <thead>
                        <tr>
                            <th class="border-top-0 border-dark">Název</th>
                            <th class="border-top-0 border-dark">Počet</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            @if($itemSum[$item->id] !== 0)
                                <tr>
                                    <td class="border-dark">{{$item->name}}</td>
                                    <td class="border-dark">{{$itemSum[$item->id]}} x</td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            @foreach($groups as $group)


                <div class="card mt-3">
                    <div class="card-header d-flex">
                        <div><b> {{$group->name}} </b></div>
                    </div>
                    <div class="card-body mt-0 pt-0 ">
                        <table class="table table-responsive-md">
                            <tbody>
                            @foreach($group->users as $user)
                                @if(count($user->day_orders)>0)
                                    <tr>
                                        <td>{{$user->name}}</td>
                                        @foreach($user->day_orders as $day_order)
                                            <td class="text-right">
                                                @foreach($day_order->items as $item)
                                                    {{$item->name}} x
                                                    {{$item->pivot->amount}} <br><br>
                                                @endforeach
                                            </td>
                                        @endforeach
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endforeach
            {{--            @foreach($orders as $order)--}}
            {{--                <div class="card mt-3">--}}
            {{--                    <div class="card-header bg-white text-dark d-flex">--}}
            {{--                        <div><b>Objednávka č. {{$order->id}}</b></div>--}}
            {{--                        <a class="float-right ml-auto btn btn-success" href="/orders/info/{{$order->id}}"--}}
            {{--                           target="_blank">Podrobnosti</a>--}}
            {{--                    </div>--}}
            {{--                    <div class="card-body mt-0 pt-0 ">--}}
            {{--                        <table class="table table-responsive-md">--}}
            {{--                            <thead>--}}
            {{--                            <tr>--}}
            {{--                                <th class="border-top-0">Název</th>--}}
            {{--                                <th class="border-top-0">Množství</th>--}}
            {{--                                <th class="border-top-0">Cena</th>--}}
            {{--                            </tr>--}}
            {{--                            </thead>--}}
            {{--                            <tbody>--}}
            {{--                            @foreach($order->items as $item)--}}
            {{--                                <tr>--}}
            {{--                                    <td>{{$item->name}}</td>--}}
            {{--                                    <td>{{$item->pivot->amount}} x</td>--}}
            {{--                                    <td>{{$item->pivot->price_full}}</td>--}}
            {{--                                </tr>--}}
            {{--                            @endforeach--}}
            {{--                            </tbody>--}}
            {{--                        </table>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            @endforeach--}}
        </div>

@endsection
