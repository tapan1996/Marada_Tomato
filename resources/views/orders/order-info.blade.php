@extends('layout.layout')

@section('content')
    <div class="card">
        <div class="card-header bg-white text-dark">
            <b>Objednávka č. {{$order->id}}</b>
        </div>
        <div class="card mt-3 w-75 ml-auto mr-auto">
            <div class="card-header bg-white text-dark d-flex">
                <b>Údaje o zákazníkovi</b>
            </div>
            <div class="card-body mt-0 pt-0 ">
                <table class="table table-responsive-md">
                    <tbody>
                    <tr>
                        <td class="border-top-0"><b>Jméno</b></td>
                        <td class="border-top-0">{{$order->user->name}}</td>
                    </tr>
                    <tr>
                        <td><b>IČO</b></td>

                        <td>{{$order->user->ico}}</td>
                    </tr>
                    <tr>
                        <td><b>DIČ</b></td>
                        <td>{{$order->user->dic}}</td>
                    </tr>
                    <tr>
                        <td><b>Telefon</b></td>
                        <td>{{$order->user->phone}}</td>
                    </tr>
                    <tr>
                        <td><b>E-mail</b></td>
                        <td>{{$order->user->email}}</td>
                    </tr>
                    <tr>
                        <td><b>Adresa</b></td>
                        <td>{{$order->user->address}}
                            , {{$order->user->postal}} {{$order->user->city}} {{$order->user->country}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="card mt-3 w-75 ml-auto mr-auto">
            <div class="card-header bg-white text-dark d-flex">
                <b>Položky objednávky</b>
            </div>
            <div class="card-body mt-0 pt-0 ">
                <table class="table table-responsive-md">
                    <thead>
                    <tr>
                        <th class="border-top-0">Název</th>
                        <th class="border-top-0">Množství</th>
                        <th class="border-top-0">Cena</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($order->items as $item)
                        <tr>
                            <td>{{$item->name}}</td>
                            <td>{{$item->pivot->amount}} x</td>
                            <td>{{$item->pivot->price_full}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
