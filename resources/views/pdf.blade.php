<!DOCTYPE html>
<html style="font-family: Arial" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        body {
            font-family: "DejaVu Sans"
        }

        .h-100 {
            height: 100%;
        }

        .card {
            background-clip: border-box;
            border: 1px solid rgba(0, 0, 0, 0.125);
        }

        .card-header {
            padding: 10px 20px;
            margin-bottom: 0;
            background-color: rgba(0, 0, 0, 0.03);
            border-bottom: 1px solid rgba(0, 0, 0, 0.125);
        }

        .mt-3 {
            margin-top: 10px !important;
        }

        .ml-3 {
            margin-left: 10px !important;
        }

        .ml-auto {
            margin-left: auto !important;
        }

        .mr-auto {
            margin-right: auto !important;
        }

        .w-75 {
            width: 75% !important;
        }

        .table {
            border-collapse: collapse !important;
            width: 100%;
        }

        .border-top-0 {
            border-top: 0 !important;
        }

        .d-flex {
            display: flex;
        }


    </style>
</head>
<body>
<div class="card">
    <div class="card-header">
        <b>Objednávky k datu {{$date}}</b>
    </div>
    {{--    @foreach($orders as $order)--}}
    {{--        <div class="card mt-3 w-75 ml-auto mr-auto">--}}
    {{--            <div>--}}
    {{--            <div class="card-header mt-3">--}}
    {{--                <b>Objednávka č. {{$order->id}}</b>--}}
    {{--            </div>--}}
    {{--                <table class="table ml-3">--}}
    {{--                    <tbody>--}}
    {{--                    <tr>--}}
    {{--                        <td class="border-top-0"><b>Jméno:</b></td>--}}
    {{--                        <td class="border-top-0">{{$order->user->name}}</td>--}}
    {{--                    </tr>--}}
    {{--                    <tr>--}}
    {{--                        <td><b>E-mail:</b></td>--}}
    {{--                        <td>{{$order->user->email}}</td>--}}
    {{--                    </tr>--}}
    {{--                    <tr>--}}
    {{--                        <td><b>Adresa:</b></td>--}}
    {{--                        <td>{{$order->user->address}}, {{$order->user->postal}} {{$order->user->city}} {{$order->user->country}}</td>--}}
    {{--                    </tr>--}}
    {{--                    </tbody>--}}
    {{--                </table>--}}
    {{--                <table class="table mt-3">--}}
    {{--                    <thead>--}}
    {{--                    <tr>--}}
    {{--                        <th>Název</th>--}}
    {{--                        <th>Množství</th>--}}
    {{--                        <th>Cena</th>--}}
    {{--                    </tr>--}}
    {{--                    </thead>--}}
    {{--                    <tbody>--}}
    {{--                    @foreach($order->items as  $item)--}}
    {{--                        <tr>--}}
    {{--                            <td></td>--}}
    {{--                            <td>{{$item->name}}</td>--}}
    {{--                            <td>{{$item->pivot->amount}} x</td>--}}
    {{--                            <td>{{$item->pivot->price_full}}</td>--}}
    {{--                        </tr>--}}
    {{--                    @endforeach--}}
    {{--                    <tr>--}}
    {{--                        <td></td>--}}
    {{--                        <td><b>Celková cena:</b></td>--}}
    {{--                        <td><b>{{$order->price_full}}</b></td>--}}
    {{--                    </tr>--}}
    {{--                    </tbody>--}}
    {{--                </table>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--@endforeach--}}




    @foreach($groups as $group)


        <div class="card mt-3">
            <div class="card-header bg-white text-dark">
                <div><b> {{$group->name}} </b></div>
            </div>
            <div class="card-body mt-0 pt-0 ">
                <table class="table table-responsive-md">
                    <tbody>
                    @foreach($group->users as $user)
                        @if(count($user->day_orders)>0)
                            <tr>
                                <td style="text-align: center;border:solid grey 1px">
                                    {{$user->name}}<br>
                                    {{$user->price}} Kč
                                </td>
                                <td style="border:solid grey 1px;text-align: center">{{$user->city}}<br>
                                    {{$user->address}}

                                </td>
                                @foreach($user->day_orders as $day_order)
                                    <td style="text-align: right;border:solid grey 1px" class="text-right">
                                        @foreach($day_order->items as $item)
                                            {{$item->name}} x
                                            {{$item->pivot->amount}} <br>
                                        @endforeach
                                    </td>
                                    a
                                @endforeach

                            </tr>

                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endforeach

</div>
</body>
</html>
