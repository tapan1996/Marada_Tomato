<?php

namespace App\Http\Controllers;

use App\Backend\AresApi;
use App\Mail\InviteCustomer;
use App\Models\Group;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsersController extends Controller
{
    public function index()
    {
        return view('users.index', ['users' => User::withTrashed()->get()]);
    }


    public function create(Request $request)
    {

        $groups = Group::all();


        return view('users.create', ['groups' => $groups, 'users' => User::all()]);

    }


    public function store(Request $request)
    {
        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->group_id = $request->input('group_id');
        $user->password = Hash::make($request->input('password'));
        $user->hash = Str::random(20);
        $user->dealer_id = $request->input('dealer_id');

//        $user->ico = $request->input('ico');
//        $user->type = $request->input('type');
//        $user->address = $request->input('address');
//        $user->city = $request->input('city');
//        $user->postal = $request->input('postal');
//        $user->country = $request->input('country');
//        $user->company = $request->input('company');
//        $user->dic = $request->input('dic');
//        $user->phone = $request->input('phone');
        $user->save();


        Mail::to($user->email)->send(new InviteCustomer($user->hash));


        return redirect(route('users.index'));
    }


    public function edit($id)
    {
        $groups = Group::all();

        return view('users.edit', ['user' => User::findOrFail($id), 'groups' => $groups, 'dealers' => User::all()]);
    }


    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->ico = $request->input('ico');
        $user->address = $request->input('address');
        $user->city = $request->input('city');
        $user->postal = $request->input('postal');
        $user->country = $request->input('country');
        $user->company = $request->input('company');
        $user->group_id = $request->input('group_id');
        $user->dealer_id = $request->input('dealer_id');
        $user->dic = $request->input('dic');
        $user->phone = $request->input('phone');
        $user->save();
        return redirect(route('users.index'));
    }


    public function customerInvitationAccepted(Request $request, $hash)
    {
        $user = User::where('hash', '=', $hash)->first();

        if (empty($user)) {
            return "ERROR 404 -- this registration is invalid";
        }
        else {
            if ($request->input('ico')) {
                $ico = $request->input('ico');
                $aapi = new AresApi();
                $ares = $aapi->index($ico);
                return view('users.invite')->with(['ares' => $ares[0], 'user' => $user]);

            }
            else {
                return view('users.invite', ['ares' => null, 'user' => $user]);
            }
        }


    }

    public function userActivate($hash)
    {
        $user = User::where('hash', '=', $hash)->first();
        if (empty($user)) {
            return "ERROR 404 -- this registration is invalid";
        }
        else {
            $user->hash = null;
            $user->save();
            return redirect('/');
        }
    }


    public function customerInvitationStore($hash, Request $request)
    {
        $user = User::where('hash', $hash)->first();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->ico = $request->input('ico');
        $user->address = $request->input('address');
        $user->city = $request->input('city');
        $user->postal = $request->input('postal');
        $user->country = $request->input('country');
        $user->hash = null;
        $user->company = $request->input('company');
        $user->dic = $request->input('dic');
        $user->phone = $request->input('phone');
        $user->save();
        return redirect('/');
    }

    public function delete($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('/users');
    }

    public function activate($id)
    {
        $user = User::where('id', $id)->withTrashed()->first();
        $user->restore();
        return redirect('/users');
    }


}
