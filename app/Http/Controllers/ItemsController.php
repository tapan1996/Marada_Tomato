<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\GroupItem;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class ItemsController extends Controller
{
    public function index()
    {
        return view('items.index', ['items' => Item::all()]);
    }


    public function create()
    {
        return view('items.create');
    }


    public function store(Request $request)
    {
        $item = new Item();
        $item->name = $request->input('name');
        if ($request->file('photo')) {
            $item->photo_path = $this->createPhoto($request->file('photo'));
        }
        $item->save();

        foreach (Group::all() as $group) {
            $g_item = new GroupItem();
            $g_item->item_id = $item->id;
            $g_item->group_id = $group->id;
            $g_item->price_full = 0;
            $g_item->price = 0;
            $g_item->save();
        }


        return redirect(route('items.index'));
    }


    public function edit($id)
    {

        return view('items.edit', ['item' => Item::findOrFail($id)]);
    }


    public function update(Request $request, $id)
    {
        $item = Item::findOrFail($id);
        $item->name = $request->input('name');
        if ($request->file('photo')) {
            $item->photo_path = $this->createPhoto($request->file('photo'));
        }
        $item->save();
        return redirect(route('items.index'));
    }


    public function destroy($id)
    {
        //
    }


    public function createPhoto(UploadedFile $file)
    {
        $image_name = $file->hashName() . '.' . $file->getClientOriginalExtension();
        $file->storeAs('public/photos', $image_name);
        return '/storage/photos/' . $image_name;
    }


}
