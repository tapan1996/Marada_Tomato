<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\GroupItem;
use App\Models\Item;
use Illuminate\Http\Request;

class GroupsController extends Controller
{

    public function index()
    {
        return view('groups.index', ['groups' => Group::all()]);
    }


    public function create()
    {
        return view('groups.create');
    }


    public function store(Request $request)
    {
        $group = new Group();
        $group->name = $request->input('name');
        $group->save();

        foreach (Item::all() as $item) {
            $g_item = new GroupItem();
            $g_item->item_id = $item->id;
            $g_item->group_id = $group->id;
            $g_item->price_full = 0;
            $g_item->price = 0;
            $g_item->save();
        }


        return redirect(route('groups.index'));
    }


    public function edit($id)
    {
        $items = Item::all();
        $item_prices = [];
        foreach ($items as $item) {
            if (GroupItem::where('item_id', $item->id)->where('group_id', $id)->first()) {


                $item_prices[$item->id] = GroupItem::where('item_id', $item->id)->where('group_id', $id)->first()->price;
            }

            else {
                $item_prices[$item->id] = 0;
            }
        }

        return view('groups.edit', ['group' => Group::findOrFail($id), 'items' => $items, 'item_prices' => $item_prices]);
    }


    public function update(Request $request, $id)
    {


        $group = Group::findOrFail($id);
        $group->name = $request->input('name');
        $group->save();


        foreach ($request->input('item_price') as $item_id => $price) {

            if (GroupItem::where('item_id', $item_id)->where('group_id', $group->id)->first()) {
                $group_item = GroupItem::where('item_id', $item_id)->where('group_id', $group->id)->first();
            }
            else {
                $group_item = new GroupItem();
            }
            $group_item->group_id = $group->id;
            $group_item->item_id = $item_id;
            $group_item->price = $price;
            $group_item->save();


        }

        return redirect(route('groups.index'));
    }


    public function destroy($id)
    {
        //
    }
}
