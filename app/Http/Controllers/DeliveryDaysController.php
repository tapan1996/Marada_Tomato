<?php

namespace App\Http\Controllers;

use App\Models\DeliveryDay;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Http\Request;

class DeliveryDaysController extends Controller
{
    public function index()
    {
        $delivery_days = DeliveryDay::all();
        return view('delivery_days.index', ['delivery_days' => $delivery_days]);
    }

    public function store(Request $request)
    {


        if (DeliveryDay::all()->pluck('date')->contains($request->input('date'))) {
            return redirect('/delivery_days');
        }
        $delivery_day = new DeliveryDay();
        $delivery_day->date = $request->input('date');
        $delivery_day->save();


        foreach (Order::all() as $order) {


            if ($order->times != null) {


                $order_new = new Order();
                $order_new->price = 0;
                $order_new->price_full = 0;
                $order_new->user_id = $order->user_id;
                $order_new->date = $request->input('date');
                $order_new->save();
                foreach ($order->items as $item) {
                    $order_item = new OrderItem();
                    $order_item->item_id = $item->id;
                    $order_item->order_id = $order_new->id;
                    $order_item->amount = $item->pivot->amount;
                    $order_item->price = $item->pivot->price;
                    $order_item->price_full = $item->pivot->price_full;
                    $order_item->save();
                }
                $order->times = $order->times - 1;
                if ($order->times === 0) {
                    $order->times = null;
                }
                $order->save();
            }
        }
        return redirect('/delivery_days');

    }

    public function delete($id)
    {
        $delivery_day = DeliveryDay::findOrFail($id);
        $delivery_day->delete();
        return redirect('/delivery_days');

    }
}
