<?php

namespace App\Http\Controllers;

use App\Models\DeliveryDay;
use App\Models\GroupItem;
use App\Models\Item;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{
    public function index()
    {
        $user = User::findOrFail(Auth::id());
        if ($user->dealer_id) {
            $group_id = User::findOrFail(User::findOrFail(Auth::id())->dealer_id)->group_id;
        }
        else {
            $group_id = User::findOrFail(Auth::id())->group_id;
        }

        $items = Item::all();

        foreach ($items as $item) {
            $item->price_full = GroupItem::where('group_id', $group_id)->where('item_id', $item->id)->first()->price_full;
            $item->price = GroupItem::where('group_id', $group_id)->where('item_id', $item->id)->first()->price;
        }


        $dates = DeliveryDay::whereDate('date', '>=', Carbon::now()->add(2, 'day'))->get()->pluck('date');

        return view('customer.index', ['items' => $items, 'dates' => collect($dates), 'orders' => $user->orders]);
    }
}
