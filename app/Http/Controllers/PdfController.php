<?php


namespace App\Http\Controllers;


use App\Models\Group;
use App\Models\Order;

//use Barryvdh\DomPDF\PDF;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;


class PdfController extends Controller

{

    public function create(Request $request)
    {
        $date = $request->input('date');
        $orders = Order::whereDate('date', $date)->get();

        $date_show = Carbon::make($date)->format('d. m. yy');
        $date_name = Carbon::make($date)->format('d-m-yy');


        $groups = Group::all();
        $orders_all = Order::whereDate('date', $date)->get();
        foreach ($groups as $group) {
            foreach ($group->users as $user) {
                $user->day_orders = [];
                if ($user->dealer_id === null) {


                    $user->day_orders = Order::where('user_id', $user->id)->whereDate('date', $date)->get();
                    $price = 0;
                    foreach ($orders_all as $order) {
                        if ($order->user->dealer_id === $user->id) {
                            $user->day_orders = $user->day_orders->push($order);
                            $price = $price + $order->price;
                        }
                    }
                    $user->price = $price;
                }
            }
        }


        $data = ['orders' => $orders, 'date' => $date_show, 'groups' => $groups];


        //  $html = view('pdf', $data)->render();
        //    $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
        //return PDF::loadHTML($html, 'UTF-8')->show();
        //  $pdf = App::make('dompdf.wrapper');
        //   $pdf->loadHTML($html, 'utf-8');

        $pdf = PDF::loadView('pdf', $data, $data, 'utf-8');
        // $pdf = PDF::loadHTML($html, 'utf-8');


        return $pdf->download('Objednávky_' . $date_name . '.pdf');

    }

}
