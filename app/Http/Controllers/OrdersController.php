<?php

namespace App\Http\Controllers;

use App\Models\DeliveryDay;
use App\Models\Group;
use App\Models\GroupItem;
use App\Models\Item;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{
    public function show()
    {

        $items = Item::all();
        $itemSum = array();


        if (!DeliveryDay::whereDate('date', '>=', Carbon::now())->get()->isEmpty()) {
            $date = Carbon::make(DeliveryDay::whereDate('date', '>=', Carbon::now())->get()[0]->date);
        }
        else {
            $date = Carbon::now();
        }

        $orders = Order::whereDate('date', $date);
        foreach ($items as $item) {
            $ordersId = Order::whereDate('date', $date)->pluck('id');

            $count = OrderItem::where('item_id', $item->id)->whereIn('order_id', $ordersId)->sum('amount');
            $itemSum[$item->id] = $count;
        }
        $groups = Group::all();
        $orders_all = Order::whereDate('date', $date)->get();
        foreach ($groups as $group) {
            foreach ($group->users as $user) {
                $user->day_orders = [];
                if ($user->dealer_id === null) {


                    $user->day_orders = Order::where('user_id', $user->id)->whereDate('date', $date)->get();

                    foreach ($orders_all as $order) {
                        if ($order->user->dealer_id === $user->id) {
                            $user->day_orders = $user->day_orders->push($order);
                        }
                    }
                }
            }
        }


        return view('orders.orders', ['orders' => $orders, 'itemSum' => $itemSum, 'items' => $items, 'date' => $date, 'groups' => $groups, 'dates' => DeliveryDay::all()]);
    }

    public function find(Request $request)
    {
        $date = $request->input('date');
        $orders = Order::whereDate('date', $date)->get();

        $items = Item::all();
        $itemSum = array();
        foreach ($items as $item) {
            $ordersId = Order::whereDate('date', $date)->pluck('id');

            $count = OrderItem::where('item_id', $item->id)->whereIn('order_id', $ordersId)->sum('amount');
            $itemSum[$item->id] = $count;
        }

        $groups = Group::all();
        foreach ($groups as $group) {
            foreach ($group->users as $user) {
                $user->day_orders = Order::where('user_id', $user->id)->whereDate('date', $date)->get();
            }
        }

        return view('orders.orders', ['orders' => $orders, 'itemSum' => $itemSum, 'items' => $items, 'groups' => $groups, 'date' => Carbon::make($date), 'dates' => DeliveryDay::all()]);
    }

    public function showInfo($id)
    {
        $order = Order::findOrFail($id);

        return view('orders.order-info', ['order' => $order]);

    }

    public function store(Request $request)
    {

        $error = true;
        foreach ($request->amount as $amount) {
            if ($amount > 0) {
                $error = false;
            }
        }
        if ($error) {
            return 'Zadejte platné množství.';
        }


        $user = User::findOrFail(Auth::id());
        $order = new Order();
        $order->user_id = Auth::id();
        $order->date = Carbon::make($request->input('date'));
        $order->times = $request->input('times');
        $order->user_id = Auth::id();
        $order->price_full = $this->countPriceFull($request->amount, $user, $request->input('times'));
        $order->price = $this->countPrice($request->amount, $user, $request->input('times'));
        $order->save();

        $this->attachItems($request->amount, $user, $order->id);

        if ($order->times > 0) {
            $this->storeNextDeliveryDaysOrder($order);
        }


        return 'Vaše objednávka byla odeslána ke zpracování.';

    }

    private function countPriceFull($amounts, $user, $times)
    {

        $group = Group::findOrFail($user->group_id);
        $price_full = 0;
        foreach ($amounts as $key => $amount) {
            if ($amount != 0) {
                $group_item = GroupItem::where('group_id', $group->id)->where('item_id', $key)->first();
                $price_full = $price_full + $group_item->price_full;
            }
        }
        if ($times) {
            $price_full = (1 + $times) * $price_full;
        }
        return $price_full;
    }

    private function countPrice($amounts, $user, $times)
    {
        $group = Group::findOrFail($user->group_id);
        $price = 0;
        foreach ($amounts as $key => $amount) {
            if ($amount != 0) {
                $group_item = GroupItem::where('group_id', $group->id)->where('item_id', $key)->first();
                $price = $price + $group_item->price;
            }
        }
        if ($times) {
            $price = (1 + $times) * $price;
        }
        return $price;


    }

    private function attachItems($amounts, $user, $order_id)
    {
        $group = Group::findOrFail($user->group_id);
        foreach ($amounts as $key => $amount) {
            if ($amount != 0) {
                $group_item = GroupItem::where('group_id', $group->id)->where('item_id', $key)->first();
                $order_item = new OrderItem();
                $order_item->item_id = $key;
                $order_item->order_id = $order_id;
                $order_item->amount = $amount;
                $order_item->price = $group_item->price;
                $order_item->price_full = $group_item->price_full;
                $order_item->save();
            }
        }
    }

    private function storeNextDeliveryDaysOrder($order)
    {
        $delivery_days = DeliveryDay::whereDate('date', '>', $order->date)->get();


        foreach ($delivery_days as $day) {
            if ($order->times > 0) {


                $order_new = new Order();
                $order_new->price = 0;
                $order_new->price_full = 0;
                $order_new->user_id = $order->user_id;
                $order_new->date = $day->date;
                $order_new->save();

                foreach ($order->items as $item) {
                    $order_item = new OrderItem();
                    $order_item->item_id = $item->id;
                    $order_item->order_id = $order_new->id;
                    $order_item->amount = $item->pivot->amount;
                    $order_item->price = $item->pivot->price;
                    $order_item->price_full = $item->pivot->price_full;
                    $order_item->save();

                }


                $order->times = $order->times - 1;
                if ($order->times === 0) {
                    $order->times = null;
                }
                $order->save();


            }


        }


    }
}
