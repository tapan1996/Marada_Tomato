<?php


namespace App\Backend;
use App\Http\Controllers\api\Auth\Controller;
use Illuminate\Contracts\Routing\ResponseFactory;


use Throwable;

class AresApi
{
    /**
     *
     * Fetch ARES data for specified IČO   ic required searched IČO Example: 07161719
     *
     * @param $ic
     * @return array
     *
     */
    public function index($ic)
    {
        $arrContextOptions=array(
            'ssl' => [
                'verify_peer'      => false,
                'verify_peer_name' => false
            ]
        );




//        $arrContextOptions = stream_context_create(['ssl' => [
//            'capture_session_meta' => TRUE
//        ]]);

        try
        {

            $data  = [];

            $ares_url = 'https://wwwinfo.mfcr.cz/cgi-bin/ares/darv_bas.cgi?ico=' . $ic . '';
            try
            {

                $raw_data = $this->file_get_contents_curl($ares_url);
            } catch (Throwable $exception)
            {
                $raw_data = "";
            }

            $osekane_data   = $this->get_string_between($raw_data, "<are:Odpoved>", "</are:Odpoved>");
            $bez_spec_znaku = str_replace("\n", "", $osekane_data);
            $ico            = $ic;


            // If exist - je platce dph
            try
            {
                $dic = $this->get_string_between($bez_spec_znaku, "<D:DIC zdroj=\"DPH\">", "</D:DIC>");
                if ($dic == "")
                {
                    $dic  = "Neplátce DPH";
                    $dico = "Neplátce DPH";
                }
                else
                {
                    $dico = $dic;
                }
            } catch (Throwable $exception)
            {
                $dic  = "Neplátce DPH";
                $dico = "Neplátce DPH";
            }


            $addr       = $this->get_string_between($bez_spec_znaku, "<D:OF", "</D:OF>");
            $addr_array = explode('>', $addr);

            $jmeno_firmy = $addr_array[1];
            $jmeno_firmy = htmlspecialchars_decode($jmeno_firmy);

            $typ = $addr = $this->get_string_between($bez_spec_znaku, "<D:KPF>", "</D:KPF>");

            //Detekce osvc / spolecnost
            if ($typ == 101)
            {
                $fyzik = 1;
            }
            else
            {
                $fyzik = 0;
            }

            $kraj = $addr = $this->get_string_between($bez_spec_znaku, "<D:NOK>", "</D:NOK>");

            //Pokud je to krajske mesto, tak NOK neni uvedeno, tim padem NOK=N
            $mesto = $addr = $this->get_string_between($bez_spec_znaku, "<D:N>", "</D:N>");

            $mestska_cast  = $addr = $this->get_string_between($bez_spec_znaku, "<D:NCO>", "</D:NCO>");
            $ulice         = $addr = $this->get_string_between($bez_spec_znaku, "<D:NU>", "</D:NU>");
            $cislo_popisne = $addr = $this->get_string_between($bez_spec_znaku, "<D:CD>", "</D:CD>");

            //Ne vzdy existuje
            $cislo_orientacni = $addr = $this->get_string_between($bez_spec_znaku, "<D:CO>", "</D:CO>");

            $psc = $addr = $this->get_string_between($bez_spec_znaku, "<D:PSC>", "</D:PSC>");


            if ($ulice == "")
            {
                if ($mestska_cast != "")
                {
                    $ulice = $mestska_cast;
                }
                else
                {
                    $ulice = $mesto;

                }
            }

            if ($cislo_orientacni != "")
            {
                $ulice = $ulice . " " . $cislo_popisne . "/" . $cislo_orientacni;
            }
            else
            {
                $ulice = $ulice . " " . $cislo_popisne;
            }


            if ($fyzik != 1)
            {
                //Pro zjisteni jednatele ...
                $justice_url = 'https://or.justice.cz/ias/ui/rejstrik-$firma?ico=' . $ic;

                try
                {
                    $raw_data = $this->file_get_contents_curl($justice_url);

                } catch (Throwable $exception)
                {
                    $raw_data = "";

                }


                try
                {
                    $osekane_data = $this->get_string_between($raw_data, "rejstrik-firma.vysledky?subjektId=", "&amp;typ=PLATNY");

                    $justice_id    = $osekane_data;
                    $justice_url_2 = 'https://or.justice.cz/ias/ui/rejstrik-firma.vysledky?subjektId=' . $justice_id . '&typ=PLATNY';

                } catch (Throwable $exception)
                {


                }


                try
                {
                    $raw_data = $this->file_get_contents_curl($justice_url_2);
                } catch (Throwable $exception)
                {
                    echo $exception;

                    $raw_data = "";
                }

                try
                {

                    $osekane_data   = $this->get_string_between($raw_data, "<div class=\"vypis-header print\">", "<p class=\"fr noprint\">");
                    $bez_spec_znaku = preg_replace('/[\x00-\x1F\x7F\xA0]/u', '', $osekane_data);

                    $jednatel_data  = $this->get_string_between($bez_spec_znaku, "jednatel:", "zapsáno");
                    $jednatel       = $this->get_string_between($jednatel_data, "<span><div><div><span>", "</span>");
                    $jednatel_array = explode(' ', $jednatel);

                    // TODO: pridat optimalizaci na ziskavani jmen u akciovek (neparsovat jednatele, ale nejspis cleny predstavenstva nebo tak neco), podobne u spolku,ustavu,...
                    $jmeno_jednatele    = ucfirst(mb_strtolower($jednatel_array[0]));
                    $prijmeno_jednatele = ucfirst(mb_strtolower($jednatel_array[count($jednatel_array) - 1]));

                } catch (Throwable $exception)
                {

                }

            }


            if ($fyzik == 1)
            {
                $getjmeno = explode(' ', trim($jmeno_firmy));
                $name     = $getjmeno[0];
                $surname  = $getjmeno[1];
                $type     = 1;
            }
            else
            {
                $type = 2;
            }


            if ($fyzik == 1)
            {
                $data[] = [
                    'ic'      => $ico,
                    'dic'     => $dico,
                    'town'    => $mesto . "," . $mestska_cast,
                    'street'  => $ulice,
                    'company' => $jmeno_firmy,
                    'country' => 'Česká Republika',
                    'type'    => $type,
                    'name'    => $name,
                    'surname' => $surname,
                    'postal'  => $psc,
                ];
            }
            else
            {
                $data[] = [
                    'ic'      => $ico,
                    'dic'     => $dico,
                    'town'    => $mesto . "," . $mestska_cast,
                    'street'  => $ulice,
                    'company' => $jmeno_firmy,
                    'country' => 'Česká Republika',
                    'type'    => $type,
                    'name'    => $jmeno_jednatele,
                    'surname' => $prijmeno_jednatele,
                    'postal'  => $psc,
                ];
            }

            // returns 200 (data)
            return $data;
        } catch (Throwable $exception)
        {
            $data[] = [
                'ic'      => "",
                'dic'     => "",
                'town'    => "",
                'street'  => "",
                'company' => "",
                'country' => 'Česká Republika',
                'type'    => "",
                'name'    => "",
                'surname' => "",
                'postal'  => "",
            ];
                // returns 404 (data)

            return $data;

        }


    }
    private function file_get_contents_curl( $url ) {

        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_AUTOREFERER, TRUE );
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt( $ch, CURLOPT_HEADER, 0 );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, TRUE );

        $data = curl_exec( $ch );
        curl_close( $ch );

        return $data;

    }

    public function get_string_between($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini    = strpos($string, $start);
        if ($ini == 0)
        {
            return '';
        }
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;

        return substr($string, $ini, $len);
    }
}
