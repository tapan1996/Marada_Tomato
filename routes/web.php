<?php

use App\Http\Controllers\GroupsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();


Route::get('/invite/customer/{hash}', [App\Http\Controllers\UsersController::class, 'customerInvitationAccepted']);
Route::post('/users/customer/{hash}', [App\Http\Controllers\UsersController::class, 'customerInvitationAccepted']);
Route::post('/users/customer/{hash}/submit', [App\Http\Controllers\UsersController::class, 'customerInvitationStore']);


Route::group(['middleware' => ['auth']], function () {


    Route::group(['middleware' => ['admin']], function () {
        Route::get('/', function () {
            return view('welcome');
        });
        Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
        Route::post('/users/create', [App\Http\Controllers\UsersController::class, 'create']);
        Route::get('/users/create', [App\Http\Controllers\UsersController::class, 'create']);
        Route::get('/users/edit/{id}', [App\Http\Controllers\UsersController::class, 'edit']);
        Route::post('/users/store', [App\Http\Controllers\UsersController::class, 'store']);
        Route::post('/users/update/{id}', [App\Http\Controllers\UsersController::class, 'update']);
        Route::get('/users/delete/{id}', [App\Http\Controllers\UsersController::class, 'delete']);
        Route::get('/users/activate/{id}', [App\Http\Controllers\UsersController::class, 'activate']);


        Route::get('/groups', [App\Http\Controllers\GroupsController::class, 'index'])->name('groups.index');
        Route::get('/groups/create', [App\Http\Controllers\GroupsController::class, 'create']);
        Route::get('/groups/edit/{id}', [App\Http\Controllers\GroupsController::class, 'edit']);
        Route::post('/groups/store', [App\Http\Controllers\GroupsController::class, 'store']);
        Route::post('/groups/update/{id}', [App\Http\Controllers\GroupsController::class, 'update']);

        Route::get('/items', [App\Http\Controllers\ItemsController::class, 'index'])->name('items.index');
        Route::get('/items/create', [App\Http\Controllers\ItemsController::class, 'create']);
        Route::get('/items/edit/{id}', [App\Http\Controllers\ItemsController::class, 'edit']);
        Route::post('/items/store', [App\Http\Controllers\ItemsController::class, 'store']);
        Route::post('/items/update/{id}', [App\Http\Controllers\ItemsController::class, 'update']);


        Route::post('/pdf-create', [App\Http\Controllers\PdfController::class, 'create']);

        Route::get('/orders', [App\Http\Controllers\OrdersController::class, 'show']);
        Route::post('/orders', [App\Http\Controllers\OrdersController::class, 'find']);
        Route::get('/orders/info/{id}', [App\Http\Controllers\OrdersController::class, 'showInfo']);

        Route::get('/delivery_days', [App\Http\Controllers\DeliveryDaysController::class, 'index']);
        Route::post('/delivery_days/store', [App\Http\Controllers\DeliveryDaysController::class, 'store']);
        Route::post('/delivery_days/delete/{id}', [App\Http\Controllers\DeliveryDaysController::class, 'delete']);


        Route::get('/users', [App\Http\Controllers\UsersController::class, 'index'])->name('users.index');
    });

    Route::post('/orders/store', [App\Http\Controllers\OrdersController::class, 'store']);
    Route::get('/customer', [App\Http\Controllers\CustomerController::class, 'index'])->name('customer');
});
